# GitLab Contributors

This is the application behind http://contributors.gitlab.com.

Development of this application happens at https://gitlab.com/gitlab-com/gitlab-contributors.

Historically, source code is also hosted at https://github.com/gitlabhq/gitlab-contributors.

## Ruby

Needs Ruby 2.4.1.

## System dependencies

```
# ExecJS runtime
$ sudo apt-get install nodejs

# PostgreSQL
$ sudo apt-get install postgresql postgresql-contrib libpq-dev

# rugged
$ sudo apt-get install cmake
```

## Application setup

Assuming your user is able to create databases, for example by running

```
$ sudo -u postgres createuser --superuser $USER
```

just execute

```
$ bin/setup
```

## Developing locally

To populate an empty database:

```
bin/rails runner Repo.sync
```

If you modify the name mappings, hard-coded authors, etc., that task also
updates the credits and it does so changing as little as possible.

Sometimes you may need to rebuild the assignments, for example if the actual
heuristics change. To do that please execute

```
bin/rails runner "Repo.sync(rebuild_all: true)"
```

## Test suite

To run the test suite execute

```
$ bin/rails test
```

Recent tests may need an up to date checkout of Rails. To do so:

```
$ cd rails.git
$ git fetch
```

## How to run the tests

Use the setup script to configure your application to be able to run the tests:

```
bin/setup
```

After this you can use the following rake task:

```
bundle exec rake test
```

## GitLab Specifics

Optionally clean the database:

```
rails db:drop db:create db:schema:load
```

To use the GitLab CE repository, clone the repo and sync it to a clean database:

```
git clone --mirror git://gitlab.com/gitlab-org/gitlab-ce.git
rails runner "Repo.sync(path: 'gitlab-ce.git')"
```

## Contributing

Please see the [contribution guidelines](CONTRIBUTING.md).

## License

Released under the MIT License, Copyright (c) 2012–<i>ω</i> Xavier Noria.
