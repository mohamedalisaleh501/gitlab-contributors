class AddFirstContributionAtToContributors < ActiveRecord::Migration
  def up
    add_column :contributors, :first_contribution_at, :datetime
    Contributor.set_first_contribution_timestamps(false)
  end

  def down
    remove_column :contributors, :first_contribution_at
  end
end
